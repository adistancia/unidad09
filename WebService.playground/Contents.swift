//: Playground - noun: a place where people can play

import UIKit
import Foundation
import XCPlayground

XCPSetExecutionShouldContinueIndefinitely()

class Item: NSObject {
    
    var name: String?
    var title: String?
    var author: String?
    var thumbnail: String?
    
    init?(json: [String: Any]) {
        
        if let jsonName = json["name"] as? String {
            self.name = jsonName
        }
        
        if let jsonTitle = json["title"] as? String {
            self.title = jsonTitle
        }
        
        if let jsonAuthor = json["author"] as? String {
            self.author = jsonAuthor
        }
        
        if let jsonThumbnail = json["thumbnail"] as? String {
            self.thumbnail = jsonThumbnail
        }
    }
}


let url = URL(string: "https://www.reddit.com/top/.json?limit=10")




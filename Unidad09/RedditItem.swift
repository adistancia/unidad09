//
//  RedditItem.swift
//  Unidad09
//
//  Created by Macbook Pro on 9/18/17.
//  Copyright © 2017 Macbook Pro. All rights reserved.
//

import Foundation


class Item {
    
    var name: String?
    var title: String?
    var author: String?
    var thumbnail: String?
    
    init?(json: [String: Any]) {
        
        if let jsonName = json["name"] as? String {
            self.name = jsonName
        }
        
        if let jsonTitle = json["title"] as? String {
            self.title = jsonTitle
        }
        
        if let jsonAuthor = json["author"] as? String {
            self.author = jsonAuthor
        }
        
        if let jsonThumbnail = json["thumbnail"] as? String {
            self.thumbnail = jsonThumbnail
        }
    }
}

let sharedInstance = RedditManager()

class RedditManager {
    static let url = URL(string: "https://www.reddit.com/top/.json?limit=10")

    class func getInstance() -> RedditManager
    {
        return sharedInstance
    }
    
    func items(callbackItem: @escaping ((Item) -> Void) ) -> Void {

        // esto es una llamada asincronica. como no devuelve en el momento que la hacemos,
        // hay que pasarle una función que trabaje con los datos recibidos.
        let task = URLSession.shared.dataTask(with: RedditManager.url! ) { (data, response, error) in
            
            if let data = data {
                
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                
                if let dictionary = json as? [String: Any],
                    let dicData = dictionary["data"] as? [String: Any],
                    let dicChildren = dicData["children"] as? [[String: Any]] {
                    
                    for dicItem in dicChildren {
                        
                        if let itemData = dicItem["data"] as? [String: Any], let item = Item(json: itemData) {
                            // acá se llama a la función de manejo de datos
                            callbackItem(item)
                        }
                    }
                }
            }
        }
        task.resume()
    }
}
